package com.tvop.core;

import java.sql.Date;

public class DayInfo {
    Date giorno;
    int lavorativo;
    int dailynotify;
    int weeklynotify;
    int monthlynotify;

    public DayInfo(Date giorno, int lavorativo, int dailynotify, int weeklynotify, int monthlynotify) {
        this.giorno = giorno;
        this.lavorativo = lavorativo;
        this.dailynotify = dailynotify;
        this.weeklynotify = weeklynotify;
        this.monthlynotify = monthlynotify;
    }
    
    public Date getGiorno() {
        return giorno;
    }

    public void setGiorno(Date giorno) {
        this.giorno = giorno;
    }

    public int getLavorativo() {
        return lavorativo;
    }

    public void setLavorativo(int lavorativo) {
        this.lavorativo = lavorativo;
    }

    public int getDailynotify() {
        return dailynotify;
    }

    public void setDailynotify(int dailynotify) {
        this.dailynotify = dailynotify;
    }

    public int getWeeklynotify() {
        return weeklynotify;
    }

    public void setWeeklynotify(int weeklynotify) {
        this.weeklynotify = weeklynotify;
    }

    public int getMonthlynotify() {
        return monthlynotify;
    }

    public void setMonthlynotify(int monthlynotify) {
        this.monthlynotify = monthlynotify;
    }
}

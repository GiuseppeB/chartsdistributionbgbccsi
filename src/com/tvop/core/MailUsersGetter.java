package com.tvop.core;

import com.tvop.utils.ConfigPath;
import com.tvop.utils.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class MailUsersGetter {

    static final Logger LOGGER = LogManager.getLogger(MailUsersGetter.class.getName());
    private Connection conn = null;
    private DBConnection dbc = null;

    public List<RecipientInfo> getMailUsers(DateTime t, String schema) {
        PreparedStatement ps = null;
        List<RecipientInfo> result = new ArrayList<>();
        String sqlMailUsers = null;
        String dayOfWeek = null;
        String time;
        if (t.getHourOfDay() < 10) {
            time = "0" + t.getHourOfDay() + ":00";
        } else {
            time = t.getHourOfDay() + ":00";
        }

//        switch (t.getDayOfWeek()) {
//            case 1:
//                dayOfWeek = "monday";
//                break;
//            case 2:
//                dayOfWeek = "tuesday";
//                break;
//            case 3:
//                dayOfWeek = "wednesday";
//                break;
//            case 4:
//                dayOfWeek = "thursday";
//                break;
//            case 5:
//                dayOfWeek = "friday";
//                break;
//            case 6:
//                dayOfWeek = "saturday";
//                break;
//            case 7:
//                dayOfWeek = "sunday";
//                break;
//        }
//        sqlMailUsers
//                = "SELECT u.userid, u.email, ur.roleid, nt.hournotify, nt.template "
//                + "FROM p_user u INNER JOIN p_userrole ur ON u.userid = ur.userid "
//                + "INNER JOIN p_notifytable nt ON u.userid = nt.userid "
//                + "WHERE nt.hournotify = ? "
//                + "AND nt.enable = true "
//                + "AND u.notify = true "
//                + "AND " + dayOfWeek + " = true ";
        sqlMailUsers
                = "SELECT email "
                + "FROM p_maillist ";

        try {
            ResultSet res = null;
            dbc = new DBConnection(ConfigPath.SINK.getPath());
            conn = dbc.getConnection();

            conn.createStatement().execute("SET search_path TO " + schema);

            try (PreparedStatement s = conn.prepareStatement(sqlMailUsers);) {
                res = s.executeQuery();

                while (res.next()) {
                    RecipientInfo recipient = new RecipientInfo(res.getString("email"), res.getString("email"), "", "", "ALL");
                    result.add(recipient);
                }
            } catch (SQLException e) {
                LOGGER.error("Error getting mail users " + e.getMessage() + " " + e.getSQLState());
            } finally {
                if (res != null) {
                    res.close();
                }
                if (ps != null) {
                    ps.close();
                }
                conn.close();
                dbc.closeDatasource();
            }
        } catch (SQLException e) {
            LOGGER.error("Error trying to connect to get mail users " + e.getMessage() + " " + e.getSQLState());
        } catch (Exception e) {
            LOGGER.error("Error trying to connect to get mail users " + e.toString());
        }
        LOGGER.info("SIZE " + result.size());
        return result;
    }
}

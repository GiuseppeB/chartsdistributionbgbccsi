package com.tvop.core;

import com.tvop.utils.AppConfig;
import com.tvop.utils.ConfigPath;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tvop.utils.TemplateEnum;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class ChartsDistributionBG {

    static final Logger LOGGER = LogManager.getLogger(ChartsDistributionBG.class.getName());
    private static AppConfig ac;
    private static DateTime today;
    private static String step;
    private static String arg;

    public static void main(String[] args) {
        today = new DateTime();
        arg = "D";
        if (args.length > 0) {
            arg = args[0];
        }
        step = "?step=" + arg;
        try {
            ObjectMapper mapper = new ObjectMapper();
            ac = mapper.readValue(new File(ConfigPath.APP.getPath()), AppConfig.class);
            MailUsersGetter mug = new MailUsersGetter();
            sendEmail(mug);
        } catch (IOException ex) {
            LOGGER.error("ERROR: " + ex.toString());
        } catch (Exception ex) {
            System.out.println(ex.toString());
            LOGGER.error("ERROR: " + ex.toString());
        }
    }

    private static void sendEmail(MailUsersGetter mug) throws Exception {
//        today = new DateTime(2018, 02, 06, 17, 15);
        // Ottengo gli utenti a cui inviare la notifica mensile
        List<RecipientInfo> mailUsers = mug.getMailUsers(today, ac.getSchema());

        int count = 0;
        // Ottengo lo screenshot da inviare via mail
        // Se l'acquisizione dello screenshot è andata a buon fine invio la mail
        if (getAllTemplate()) {
            String subject = ac.getEmailSubjectDay();
            if (arg.equals("W")) {
                subject = ac.getEmailSubjectWeek();
            } else if (arg.equals("M")) {
                subject = ac.getEmailSubjectMonth();
            }
//            RecipientInfo recipient = new RecipientInfo("Battiato", "giuseppe.battiato@technevalue.com", "R01", "17:00", "ALL");
//            Mailer mailer = new Mailer(subject, recipient, ac.getFromEmailAddress(), ac.getPathScreenshot(), ac.getScreenshotFileName());
//            mailer.sendMessage(today);
//            recipient = new RecipientInfo("Battiato", "geros.formos@gmail.com", "R01", "17:00", "ALL");
//            mailer = new Mailer(subject, recipient, ac.getFromEmailAddress(), ac.getPathScreenshot(), ac.getScreenshotFileName());
//            mailer.sendMessage(today);
            for (RecipientInfo recipient : mailUsers) {
                Mailer mailer = new Mailer(subject, recipient, ac.getFromEmailAddress(), ac.getPathScreenshot(), ac.getScreenshotFileName());
                mailer.sendMessage(today);
                count++;
            }
        }

        LOGGER.info(count + " email sent");
    }

    private static boolean getAllTemplate() {
        PhantomScreenshotGetter pjs = new PhantomScreenshotGetter();
        try {
            pjs.getScreenshot(today, TemplateEnum.ALL, step);
//            pjs.getScreenshot(today, TemplateEnum.AG);
//            pjs.getScreenshot(today, TemplateEnum.VAG);
//            pjs.getScreenshot(today, TemplateEnum.COS);
//            pjs.getScreenshot(today, TemplateEnum.COT);
        } catch (Exception ex) {
            LOGGER.error("Error get template: " + ex);
            return false;
        }
        return true;
    }
}

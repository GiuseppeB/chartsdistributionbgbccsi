package com.tvop.core;

import java.util.List;

public class RecipientInfo {
    String user;
    String email;
    String roleid;
    String hournotify;
    String template;
    List<String> resourceid;

    public RecipientInfo(String user, String email, String roleid, String hournotify, String template) {
        this.user = user;
        this.email = email;
        this.roleid = roleid;
        this.hournotify = hournotify;
        this.template = template;
    }
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public List<String> getResourceid() {
        return resourceid;
    }

    public void setResourceid(List<String> resourceid) {
        this.resourceid = resourceid;
    }

    public String getHournotify() {
        return hournotify;
    }

    public void setHournotify(String hournotify) {
        this.hournotify = hournotify;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}

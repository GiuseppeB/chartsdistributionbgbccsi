package com.tvop.core;

import com.tvop.utils.NotifyEnum;
import com.tvop.utils.MonthNameBuilder;
import com.tvop.utils.TemplateEnum;
import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class Mailer {
    static final Logger logger = LogManager.getLogger(Mailer.class.getName());
    
    private static final String SMTP_HOST_NAME = "smtp.4proodos.com";
    private static final int SMTP_HOST_PORT = 25;
    private static final String SMTP_AUTH_USER = "reports@4proodos.com";
    private static final String SMTP_AUTH_PWD  = "abc.ABC.123";
    
    private Session session = null;

    private String mailSubject;
    private RecipientInfo recipient;
    private String fromEmailAddress;
    private String pathScreenshot;
    private String screenshotFileName;

    public Mailer() {
    }

    Mailer(String mailSubject, RecipientInfo recipient, String fromEmailAddress, String pathScreenshot, String screenshotFileName) {
        this.mailSubject = mailSubject;
        this.recipient = recipient;
        this.fromEmailAddress = fromEmailAddress;
        this.pathScreenshot = pathScreenshot;
        this.screenshotFileName = screenshotFileName;
    }

    private void openSession() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
//        props.put("mail.smtp.port", "25");

        session = Session.getInstance(props);
    }

    public boolean sendMessage(DateTime today) {
        Integer day = today.getDayOfMonth();
        Integer monthNumber = today.getMonthOfYear();
        Integer year = today.getYear();
        Integer hour = today.getHourOfDay();
        String filename = "";
        MonthNameBuilder mnb = new MonthNameBuilder(monthNumber);
        String monthName = mnb.getMonthName(monthNumber);
        String subject = "";
        TemplateEnum te = TemplateEnum.fromString(recipient.template);
        
        logger.info("Sending email " + recipient.getUser());
        System.out.println("Sending email");
        
//        switch (notify) {
//            case MONTHLY: {
//                screenshotFileName += recipient.getUser() + "month" + day.toString() + "-" + monthNumber.toString() + "-" + year.toString() + ".png";
//                filename = monthlyScreenshotPathArchive + screenshotFileName;
//                subject = monthlySubject + day.toString() + " " + monthName + " " + year.toString();
//                
//                break;
//            }
//            case WEEKLY: {
//                screenshotFileName += recipient.getUser() + "week" + day.toString() + "-" + monthNumber.toString() + "-" + year.toString() + ".png";
//                filename = weeklyScreenshotPathArchive + screenshotFileName;
//                subject = weeklySubject + day.toString() + " " + monthName + " " + year.toString();
//
//                break;
//            }
//            case DAILYAFT: {
//                screenshotFileName += recipient.getUser() + "aft" + day.toString() + "-" + monthNumber.toString() + "-" + year.toString() + ".png";
//                filename = afternoonScreenshotPathArchive + screenshotFileName;
//                subject = dailySubject + day.toString() + " " + monthName + " " + year.toString();
//
//                break;
//            }
//            case DAILYEV: {
//                screenshotFileName += recipient.getUser() + "ev" + day.toString() + "-" + monthNumber.toString() + "-" + year.toString() + ".png";
//                filename = eveningScreenshotPathArchive + screenshotFileName;
//                subject = dailySubject + day.toString() + " " + monthName + " " + year.toString();
//
//                break;
//            }
//        }
        String hourStr;
        if(hour<10){
            hourStr = "0" + hour.toString() + ":00";
        }
        else{
            hourStr = hour.toString() + ":00";
        }
        screenshotFileName += te.toString() + "_" + year.toString() + "-" + monthNumber.toString() + "-" + day.toString() + "_" + hour.toString() + ".png";
        filename = pathScreenshot + screenshotFileName;
        subject = mailSubject  + day.toString() + " " + monthName + " " + year.toString() + " alle ore " + hourStr;

        try {
            openSession();

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(fromEmailAddress));
//            message.setFrom(new InternetAddress("reports@4proodos.com"));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient.getEmail()));
//            message.addRecipient(Message.RecipientType.TO, new InternetAddress("giuseppe.battiato@technevalue.com"));

            // Set Subject: header field
            message.setSubject(subject);

            message.addHeader("Content-type", "text/html; charset=utf-8");
//                message.addHeader("format", "flowed");
            message.addHeader("Content-Transfer-Encoding", "base64");

            // Create a multipar message
            Multipart multipart = new MimeMultipart("related");

            // Part two is attachment
            BodyPart messageBodyPart = new MimeBodyPart();

            String htmlText = "<img alt=\"\" src=\"cid:image\"/>";
            messageBodyPart.setContent(htmlText, "text/html");

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            messageBodyPart = new MimeBodyPart();

            // Adding the attach
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setHeader("Content-ID", "<image>");

            File f = new File(filename);
            // Controllo se lo screenshot è stato effettivamente acquisito
            if (f.exists() && !f.isDirectory()) {
                messageBodyPart.setFileName(screenshotFileName);
                messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                multipart.addBodyPart(messageBodyPart);

                message.setContent(multipart);

                Transport transport = session.getTransport();

                transport.connect(SMTP_HOST_NAME, SMTP_HOST_PORT, SMTP_AUTH_USER, SMTP_AUTH_PWD);
                transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
                transport.close();
                
                logger.info("Email sent " + recipient.getUser());
                System.out.println("Email sent");
                
                return true;
            } 
            else {
                logger.error(screenshotFileName + " does not exist");
                
                return false;
            }
        } catch (AddressException e) {
            logger.error("Address error: " + e.getMessage());
        } catch (MessagingException e) {
            logger.error("Messaging error: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Error sending email: " + e.getMessage());
        }
        
        return false;
    }

    void sendErrorMessage(RecipientInfo user) {
        try {
            openSession();

            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress("error@bcc.it"));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("giuseppe.battiato@technevalue.com"));

            // Set Subject: header field
            message.setSubject("Errore invio messaggio all'utente " + user.getUser());

            // Now set the actual message
            message.setText("Errore invio messaggio all'utente " + user.getUser());

            // Send message
            Transport.send(message);
        } catch (MessagingException ex) {
            logger.error("Cannot send error email " + ex.toString());
        }
    }
}

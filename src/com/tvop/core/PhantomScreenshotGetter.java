package com.tvop.core;

import com.tvop.utils.AppConfig;
import com.tvop.utils.ConfigPath;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tvop.utils.TemplateEnum;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PhantomScreenshotGetter {

    AppConfig ac;
    static final Logger LOGGER = LogManager.getLogger(PhantomScreenshotGetter.class.getName());

    public PhantomScreenshotGetter() {}

    public boolean getScreenshot(DateTime today, TemplateEnum template, String step) throws Exception {
        boolean stop = false;
        int attempt = 0;
        WebDriver driver = null;

        while (!stop) {
            LOGGER.info("Phantom starts working");
            try {
                Integer day = today.getDayOfMonth();
                Integer monthNumber = today.getMonthOfYear();
                Integer year = today.getYear();
                Integer hour = today.getHourOfDay();

                ObjectMapper mapper = new ObjectMapper();
                ac = mapper.readValue(new File(ConfigPath.APP.getPath()), AppConfig.class);
                
                String path = ac.getPathWeb();

                String screenshotFileNamePath = ac.getPathScreenshot() + ac.getScreenshotFileName();

                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setJavascriptEnabled(true);
                caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, ac.getPhantomPath());

                driver = new PhantomJSDriver(caps);
                WebDriverWait wait = new WebDriverWait(driver, 60);
                
                switch (template){
                    case ALL:
                        driver.manage().window().setSize(new Dimension(1500, 2000));
                        driver.get(path + "templateALL.xhtml" + step);
                        break;
                    case AG:
                        driver.manage().window().setSize(new Dimension(2100, 1000));
                        driver.get(path + "templateAG.xhtml" + step);
                        break;
                    case VAG:
                        driver.manage().window().setSize(new Dimension(2100, 1000));
                        driver.get(path + "templateVAG.xhtml" + step);
                        break;
                    case COS:
                        driver.manage().window().setSize(new Dimension(2100, 1000));
                        driver.get(path + "templateCOS.xhtml" + step);
                        break;
                    case COT:
                        driver.manage().window().setSize(new Dimension(2100, 1000));
                        driver.get(path + "templateCOT.xhtml" + step);
                        break;
                }

                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("analisidatatable")));
                
                // Ottengo lo screenshot
                File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

                screenshotFileNamePath += template.toString() + "_" + year.toString() + "-" + monthNumber.toString() + "-" + day.toString() + "_" + hour.toString() + ".png";

                // Salvo lo screenshot
                FileUtils.copyFile(file, new File(screenshotFileNamePath));
                stop = true;
                LOGGER.info("Phantom success at attempt number " + attempt);
            } catch (IOException | WebDriverException ex) {
                attempt++;
                if (attempt == 3) {
                    stop = true;
                }

                LOGGER.error("ERROR at attempt number: " + attempt + ". " + ex.getMessage() + " " + ex.toString());

                try {
                    Thread.sleep(30000);
                } catch (InterruptedException ex1) {
                    LOGGER.error("ERROR: " + ex1.getMessage() + " " + ex1.toString());
                }
            } catch (Exception e) {
                LOGGER.error("ERROR: " + e.toString());
            } finally {
                // Chiudo il browser
                if (driver != null) {
                    driver.quit();
                }
            }

        }

        if (attempt == 3) {
            LOGGER.error("Reached max attempts number for user ");
//            Mailer mailer = new Mailer();
//            mailer.sendErrorMessage(user);
            return false;
        } else {
            return true;
        }
    }
}

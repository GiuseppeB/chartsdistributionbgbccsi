package com.tvop.utils;

public enum TemplateEnum {
    ALL("ALL"),
    AG("Agent Group"),
    VAG("Virtual Agent Group"),
    COS("Flussi COS"),
    COT("Flussi COT");
    
    private final String template;

    TemplateEnum(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public static TemplateEnum fromString(String value) {
        for (TemplateEnum b : TemplateEnum.values()) {
            if (b.template.equalsIgnoreCase(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Not valid value from TemplateEnum: " + value);
    }
}

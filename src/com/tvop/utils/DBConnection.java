
package com.tvop.utils;

import com.tvop.core.ConnectionProps;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DBConnection {
    private String propsFileName = null;
    private static final Logger LOGGER = LogManager.getLogger(DBConnection.class.getName());
    private BasicDataSource ds = null;
    
    private DBConnection() {
    }

    public DBConnection(String propsFileName) {
        this.propsFileName = propsFileName;
    }

    public synchronized Connection getConnection() {
        Connection conn = null;
        ds = new BasicDataSource();
        
        try {
            ConnectionProps p = loadProps(this.propsFileName);
            ds.setDriverClassName(p.getDriverClass());
            ds.setUsername(p.getUsername());
            ds.setPassword(p.getPassword());
            ds.setUrl(p.getUrl());
            conn = ds.getConnection();
        } 
        catch (Exception e) {
            LOGGER.error("Error connecting db - " + e.toString());
            e.printStackTrace();
        }
        
        return conn;
    }

    private synchronized ConnectionProps loadProps(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(path), ConnectionProps.class);
    }

    public void closeDatasource() {
        try {
            ds.close();
            LOGGER.info("Db connection closed");
        } catch (SQLException ex) {
            LOGGER.error("Error closing connection db " + ex.getMessage());
        }
    }
}

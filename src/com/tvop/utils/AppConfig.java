package com.tvop.utils;

public class AppConfig {
    // Oggetto della mail
    private String emailSubjectDay;
    private String emailSubjectWeek;
    private String emailSubjectMonth;
    
    // Indirizzo dei destinatari
    private String recipients;
    
    // Indirizzo del mittente
    private String fromEmailAddress;
            
    // Path della cartella dell'ultimo screenshot
    private String pathScreenshot;
    
    // Prefisso dell'immagine
    private String screenshotFileName;   
    
    // Path di Phantom
    private String phantomPath;
    
    // Path app web
    private String pathWeb;
    
    // Schema
    private String schema;

    public String getEmailSubjectDay() {
        return emailSubjectDay;
    }

    public void setEmailSubjectDay(String emailSubject) {
        this.emailSubjectDay = emailSubject;
    }

    public String getEmailSubjectWeek() {
        return emailSubjectWeek;
    }

    public void setEmailSubjectWeek(String emailSubjectWeek) {
        this.emailSubjectWeek = emailSubjectWeek;
    }

    public String getEmailSubjectMonth() {
        return emailSubjectMonth;
    }

    public void setEmailSubjectMonth(String emailSubjectMonth) {
        this.emailSubjectMonth = emailSubjectMonth;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getFromEmailAddress() {
        return fromEmailAddress;
    }

    public void setFromEmailAddress(String fromEmailAddress) {
        this.fromEmailAddress = fromEmailAddress;
    }

    public String getPathScreenshot() {
        return pathScreenshot;
    }

    public void setPathScreenshot(String pathScreenshot) {
        this.pathScreenshot = pathScreenshot;
    }

    public String getScreenshotFileName() {
        return screenshotFileName;
    }

    public void setScreenshotFileName(String screenshotFileName) {
        this.screenshotFileName = screenshotFileName;
    }

    public String getPhantomPath() {
        return phantomPath;
    }

    public void setPhantomPath(String phantomPath) {
        this.phantomPath = phantomPath;
    }

    public String getPathWeb() {
        return pathWeb;
    }

    public void setPathWeb(String pathWeb) {
        this.pathWeb = pathWeb;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}

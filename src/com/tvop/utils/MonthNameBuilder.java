package com.tvop.utils;

public class MonthNameBuilder {
    private int monthNumber = 1;
    private String monthName = "";
    
    public MonthNameBuilder(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }
    
    public String getMonthName(int monthNumber){
        switch(monthNumber){
            case 1:{
                monthName = "Gennaio";
                
                break;
            }
            case 2:{
                monthName = "Febbraio";
                
                break;
            }
            case 3:{
                monthName = "Marzo";
                
                break;
            }
            case 4:{
                monthName = "Aprile";
                
                break;
            }
            case 5:{
                monthName = "Maggio";
                
                break;
            }
            case 6:{
                monthName = "Giugno";
                
                break;
            }
            case 7:{
                monthName = "Luglio";
                
                break;
            }
            case 8:{
                monthName = "Agosto";
                
                break;
            }
            case 9:{
                monthName = "Settembre";
                
                break;
            }
            case 10:{
                monthName = "Ottobre";
                
                break;
            }
            case 11:{
                monthName = "Novembre";
                
                break;
            }
            case 12:{
                monthName = "Dicembre";
                
                break;
            }
        }
        return monthName;
    }
}

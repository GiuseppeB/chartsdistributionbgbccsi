package com.tvop.utils;

public enum NotifyEnum {
    MONTHLY(1),
    WEEKLY(2),
    DAILYAFT(3),
    DAILYEV(4);
    
    private int notify;

    private NotifyEnum(int notify) {
        this.notify = notify;
    }

    public int getNotify() {
        return notify;
    }
}

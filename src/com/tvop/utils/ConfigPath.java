package com.tvop.utils;

public enum ConfigPath {
    APP("./config/AppConfig.json"),
    CALL("./config/CallSource.json"),
    TICKET("./config/TicketSource.json"),
    SINK("./config/Sink.json"),
    TAIKO("./config/Sink.json");

    private final String path;

    ConfigPath(String path) {
        this.path = path;
    }

    /** 
     * 
     * @return the path and filename containing db connection props
     */
    public String getPath() {
        return path;
    }
}
